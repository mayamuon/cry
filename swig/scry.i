
%module scry
%include "std_string.i"
%include "std_vector.i"
%include "typemaps.i"
%include "carrays.i"
%include "cpointer.i"
%include "exception.i"


%feature("autodoc", "1");

%{
#define SWIG_FILE_WITH_INIT
#include "CRYParticle.h"
#include "CRYGenerator.h"
#include "CRYSetup.h"
%}

%exception {
  try {
    $action
  } catch (const std::exception& e) {
       SWIG_exception(SWIG_RuntimeError, e.what());
  }
}

/*
namespace std {
    %template(ParticleVector) vector<CRYParticle*>;
    %apply ParticleVector *OUTPUT { vector<CRYParticle*> *retList };
}
*/


%include "CRYParticle.h"
%include "CRYGenerator.h"
%include "CRYSetup.h"
