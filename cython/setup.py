#!/usr/bin/env python
from distutils.core import setup
from distutils.extension import Extension

import os

USE_CYTHON = True

sources_simple = [['CRYSetup'],
                  ['CRYGenerator'],
                   ['CRYParticle'],
                  ]

add_path = lambda n: ('CRY/'+n+'.pyx' if USE_CYTHON else 'build/CRY/'+n+'.cpp')

sources = dict()
for source in sources_simple:
    sources['CRY.'+source[0]] = list(map(add_path,source))

extensions = [Extension(source_name,
           sources=sources[source_name],
           include_dirs=['../include', 'CRY'],
           extra_objects=['../build/libCRY.a'],
           ) for source_name in sources]

if USE_CYTHON:
    from Cython.Build import cythonize
    extensions = cythonize(extensions, build_dir='build')

setup(
  name = 'PyCRY',
  ext_modules=extensions,
)

