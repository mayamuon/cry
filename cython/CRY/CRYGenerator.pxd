
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.map cimport map
from .CRYParticle cimport CRYParticle
from .CRYSetup cimport CRYSetup
    
    
cdef extern from "CRYGenerator.h":
    cdef cppclass CRYGenerator:
        CRYGenerator(CRYSetup *setup)
        
        vector[CRYParticle*]* genEvent()
        void genEvent(vector[CRYParticle*] *retList)
        double timeSimulated()
        CRYParticle *primaryParticle()
        double boxSizeUsed()
        
cdef class Generator:
    cdef CRYGenerator *thisptr
