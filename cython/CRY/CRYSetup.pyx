#!python
#cython: embedsignature = True
#cython: language_level = 3
#distutils: language = c++

cdef class Setup:
    'A wrapper around CRYSetup'

    def __cinit__(self, str configData, str dataDir):
        self.thisptr = new CRYSetup(
                configData.replace('\n',' ').encode('ascii'),
                dataDir.encode('ascii'))

    def __delloc__(self):
        del self.thisptr
        
    def __str__(self):
        mystr = ['Setup:']
        mystr.append('returnNeutrons: {0}'.format(self.returnNeutrons))
        mystr.append('returnProtons: {0}'.format(self.returnProtons))
        mystr.append('returnGammas: {0}'.format(self.returnGammas))
        mystr.append('returnElectrons: {0}'.format(self.returnElectrons))
        mystr.append('returnMuons: {0}'.format(self.returnMuons))
        mystr.append('returnPions: {0}'.format(self.returnPions))
        mystr.append('returnKaons: {0}'.format(self.returnKaons))
        mystr.append('suboxlength: {0}'.format(self.subboxLength))
        mystr.append('altitude: {0}'.format(self.altitude))
        mystr.append('latitude: {0}'.format(self.latitude))
        mystr.append('date: {0}'.format(self.param(date)))
        mystr.append('nParticlesRange: {0[0]} -- {0[1]}'.format(self.nParticlesRange))
        mystr.append('offset: {0}'.format(self.offset))
        return '\n'.join(mystr)
        
    def param(self, CRYParms parm):
        return self.thisptr.param(parm)
    def setParam(self, CRYParms parm, double value):
        self.thisptr.setParam(parm, value)     
        
       
    property subboxLength:
        def __get__(self):
            return self.thisptr.param(subboxLength)
        def __set__(self, val):
            self.thisptr.setParam(subboxLength, val)
 
    property altitude:
        def __get__(self):
            return self.thisptr.param(altitude)
        def __set__(self, val):
            self.thisptr.setParam(altitude, val)
            
    property latitude:
        def __get__(self):
            return self.thisptr.param(latitude)
        def __set__(self, val):
            self.thisptr.setParam(latitude, val)

    property offset:
        def __get__(self):
            return (self.thisptr.param(xoffset),
                    self.thisptr.param(yoffset),
                    self.thisptr.param(zoffset))
        def __set__(self, val):
            self.thisptr.setParam(xoffset, val[0])
            self.thisptr.setParam(yoffset, val[1])
            self.thisptr.setParam(zoffset, val[2])
            
    property nParticlesRange:
        def __get__(self):
            return (self.thisptr.param(nParticlesMin),
                    self.thisptr.param(nParticlesMax))
        def __set__(self, val):
            self.thisptr.setParam(nParticlesMin, val[0])
            self.thisptr.setParam(nParticlesMax, val[1]) 
            
    property returnNeutrons:
        def __get__(self):
            return bool(self.thisptr.param(returnNeutrons))
        def __set__(self, val):
            self.thisptr.setParam(returnNeutrons, 1 if val else 0)
            
    property returnProtons:
        def __get__(self):
            return bool(self.thisptr.param(returnProtons))
        def __set__(self, val):
            self.thisptr.setParam(returnProtons, 1 if val else 0)
                        
    property returnGammas:
        def __get__(self):
            return bool(self.thisptr.param(returnGammas))
        def __set__(self, val):
            self.thisptr.setParam(returnGammas, 1 if val else 0)
                        
    property returnElectrons:
        def __get__(self):
            return bool(self.thisptr.param(returnElectrons))
        def __set__(self, val):
            self.thisptr.setParam(returnElectrons, 1 if val else 0)
                        
    property returnMuons:
        def __get__(self):
            return bool(self.thisptr.param(returnMuons))
        def __set__(self, val):
            self.thisptr.setParam(returnMuons, 1 if val else 0)
                        
    property returnPions:
        def __get__(self):
            return bool(self.thisptr.param(returnPions))
        def __set__(self, val):
            self.thisptr.setParam(returnPions, 1 if val else 0)
                        
    property returnKaons:
        def __get__(self):
            return bool(self.thisptr.param(returnKaons))
        def __set__(self, val):
            self.thisptr.setParam(returnKaons, 1 if val else 0)
