#!python
#cython: embedsignature = True
#cython: language_level = 3
#distutils: language = c++

cdef class Particle:       
    def __cinit__(self, id=None, charge=None, KE=None):
        if id is not None:
            self.thisptr = new CRYParticle( id, charge, KE)
            self.ownership = True
        else:
            self.ownership = False
    
    def __delloc__(self):
        if self.ownership and self.thisptr != NULL:
            del self.thisptr
            
    def __str__(self):
        return ('{0.name} with charge: {0.charge}\n'
                'Position: ({0.x:.2f}, {0.y:.2f}, {0.z:.2f}) m\n' +
                'Direction: ({0.u:.3f}, {0.v:.3f}, {0.w:.3f}) @ {0.ke:.0f} MeV\n' +
                'Time: {0.t:.3e} s'
        ).format(self)
            
    @staticmethod
    cdef create(CRYParticle* ptr):
        p = Particle()
        p.thisptr = ptr
        p.ownership = False
        return p
        
    @staticmethod
    cdef create_and_own(CRYParticle* ptr):
        p = Particle()
        p.thisptr = ptr
        p.ownership = True
        return p   
        
    def setPosition(self, double x, double y, double z):
        self.thisptr.setPosition(x, y, z)
    
    def setDirection(self, double u, double v, double w):
        self.thisptr.setDirection(u, v, w)
    
    def setTime(self, double t):
        self.thisptr.setTime(t)

    property ke:
        def __get__(self):
            return self.thisptr.ke()
            
    property x:
        def __get__(self):
            return self.thisptr.x()
            
    property y:
        def __get__(self):
            return self.thisptr.y()
            
    property z:
        def __get__(self):
            return self.thisptr.z()
            
    property u:
        def __get__(self):
            return self.thisptr.u()
            
    property v:
        def __get__(self):
            return self.thisptr.v()
            
    property w:
        def __get__(self):
            return self.thisptr.w()
    
    property charge:
        def __get__(self):
            return self.thisptr.charge()    
    
    property t:
        def __get__(self):
            return self.thisptr.t()

    property id:
        def __get__(self):
            return self.thisptr.id()
            
    property PDGid:
        def __get__(self):
            return self.thisptr.PDGid()
            
    property name:
        def __get__(self):
            n = self.id
            names = 'Neutron, Proton, Pion, Kaon, Muon, Electron, Gamma'
            return names.split(', ')[n]

    
    def fill(self, CRYId id, int q,
      double ke, double x, double y, double z,
      double u, double v, double w, double t):
          self.thisptr.fill(id, q, ke, x, y, z, u, v, w, t)
